# What?

This is the bridge between [JetBrains Qodana for JVM](https://www.jetbrains.com/help/qodana/2022.2/qodana-jvm.html)
and GitLab MR commentary, with some hooks for configuring the analysis via env-vars

# What Profile Names Can I Use?

With the exception of the first one, they are named according to their filename, minus `.xml`:

* `/root/.config/idea/inspection/default.xml` (named `Project Default`)
* `/root/.config/idea/inspection/empty.xml`
* (this one is **our default**, if there is no `qodana.yaml` in your project) `/root/.config/idea/inspection/qodana.recommended.full.xml`
* `/root/.config/idea/inspection/qodana.sanity.xml`
* `/root/.config/idea/inspection/qodana.starter.full.xml`

You can also include a custom inspection profile, either in your project or hosted on a URL the GitLab job can fetch

All of these are optional, so customize them only if you want different behavior for your job

They are all environment variables, which manifest in GitLab CI as `variables:`,
such as:

```yaml
my_qodana:
  variables:
    QODANA_PROFILE_NAME: "my custom one whatever"
```

### Environment Variables

* `QODANA_PROFILE_NAME` -- the name as found in the `myName` inside the .xml file
* `QODANA_PROFILE_PATH` -- if your inspection profile `.xml` is available to the container locally, use it (such as inside your project, or if we were to start including some extra ones in the GitLab job template in the future)

     **HOWEVER**, the next two are for injecting one out of tree and **SUPERSEDE THIS VALUE**

* `QODANA_PROFILE_XML_FILE_VAR` --
    this one is tricky, as it is a level of indirection away from the _actual_ type=File CI/CD variable,
    which if named `..._B64` is expected to be base64 encoded gzipped content,
    or if named `..._URL` is expected to contain a URL of the actual profile xml

    _FOR EXAMPLE:_

    ```yaml
    variables:
      # this one "points at" the next one, which likely would be defined in the Project or Group CI/CD variables
      QODANA_PROFILE_XML_FILE_VAR: INLINE_QO_B64
      # and this one is the actual profile xml, gzip, b64
      INLINE_QO_B64: |
        H4sIAAAAAAACAz2OQQ4CIQxF95yi6QFE98DehcYbGILVoFAIMJPM7a1K7Ob3N7+vNaHkWph4APtM
        Fo/cK4URC19aeUonco+JTp79gxo6BVKm/qawUuuStXjY7RFiv6YSXnSzONpCM/xdKPXDnEfydhZF
        WH1axFKuY0PQk60n3Cmj/+859QZCsWBcqwAAAA==
    ---
    # or
    variables:
      QODANA_PROFILE_XML_FILE_VAR: THE_QO_URL
      THE_QO_URL: https://example.com/my-awesome-profile.xml
    ```

* `QODANA_PROFILE_XML_FILE` -- the "hello world" version, if your profile's xml is small enough to fit inside a GitLab CI file variable


# What JDK Is This Using?

The Java 11 "JetBrains Runtime" that ships in the container.
If you wish to use a different one, the [maven toolchain mechanism](https://maven.apache.org/guides/mini/guide-using-toolchains.html)
is likely the best approach, followed by the `ProjectJdkTable` component as described by `/root/.config/idea/options/jdk.table.xml`:

```xml
    <application>
      <component name="ProjectJdkTable">
        <jdk version="2">
          <name value="jbr-11" />
          <type value="JavaSDK" />
          <version value="java version &quot;11.0.11&quot;" />
          <homePath value="/usr/lib/jvm/jbr" />
```
