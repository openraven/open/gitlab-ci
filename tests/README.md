# What?

Since [GL CI Lint](https://docs.gitlab.com/ee/api/lint.html#escape-yaml-for-json-encoding) requires _instantiated jobs_ then testing our `include:` files in the general case is hard. Thus, it's easier to just create example files in this directory which manifest the include files they're testing, and feed those to the GL API

**BE SURE** to leave the `ref:` key in these files, as they are patched during the lint phase to point at the current branch, otherwise one is not testing the right version of the file

You'll also want to watch out for "include of includes" since for our purposes they **all** need to point to the current branch/sha, and if `file-1` includes `file-0`, it'll point to the _committed_ version of that include, not the in-tree that we patched

```yaml
# file-0.yml
include:
- file: fred.yml
  ref: @patched_ref@

# file-1.yml
include:
- file: file-0.yml # <-- IS NOT TESTING THIS BRANCH's fred.yml
  ref: @patched_ref@
```

This was bootstrapped more or less via

```sh
for fn in *.yml *.yaml; do
    gojq -r --yaml-input '
    [
      [
          to_entries[]
          | select(.key|test("include|stages|variables|after_script|before_script")|not)
          | {stage: "build", extends: [.key]} + (if (((.value|type) == "object") and (.value|has("script"))) then {} else {script: "exit 0"} end)
      ]
      | to_entries[]
      | {key: "job-\(.key)", value}
    ] | from_entries' < "$fn" | json2yaml
done
```
