#!/bin/sh
set -eu
if [ -n "${TRACE:-}" ]; then set -x; fi

yamllint -c .yamllint .

my_curl() {
  curl -sfH "PRIVATE-TOKEN: $GLR_PAT" -H 'content-type: application/json;charset=utf-8' "$@"
}

to_payload() {
  jq -n --arg y "$(cat "$fn")" '{ content: $y }'
}

cd tests
rc=0

for fn in *.yml *.yaml; do
  tmp_fn="${fn}.json"
  echo "# fn=$fn" >&2
  my_curl "$CI_API_V4_URL/projects/$CI_PROJECT_ID/ci/lint?include_merged_yaml=true" \
    --data-binary "$(to_payload)" >"$tmp_fn"
  if [ "$(jq -r .valid "$tmp_fn")" != 'true' ]; then
    echo '--THE PATCHED FILE--' >&2
    cat "$fn" >&2
    echo '--THE ci/lint REPLY--' >&2
    jq < "$tmp_fn" >&2
    rc=1
  fi
  rm "$tmp_fn"
  sleep 1
done
exit $rc
