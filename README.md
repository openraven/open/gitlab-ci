# What

Reusable GitLab CI jobs via [`include: file:`](https://docs.gitlab.com/ce/ci/yaml/#includefile)

For example:

```yaml
include:
- project: openraven/open/gitlab-ci
  file: /chatops-shell.yml

# ...

my-job:
  stage: chatops
  # https://docs.gitlab.com/ce/ci/yaml/#extends
  extends:
  - .chatops-shell-functions
  script:
  - ${chat_start_fn}
  - echo hello from chatops
  - ${chat_stop_fn}
```

## Catalog

### build and push to ECR (geared toward Dockerfile with `ARG JAR_FILE`)

```yaml
include:
- project: openraven/open/gitlab-ci
  file: docker-ecr-push.yml

build and push image:
  extends:
  - .build-ecr-image
  # customizations go here, such as "no arm64" or a different docker context directory
  # variables: {}
```

### docker against dind (likely the most common)

```yaml
include:
- project: openraven/open/gitlab-ci
  file: docker-dind.yml

do some docker stuff:
  extends:
  - .docker-dind
  script:
  - docker info
  # *if* you need a before_script for some reason,
  # BE SURE to reference the existing one first
  before_script:
  - !reference [.docker-dind, before_script]
  - echo now you can do stuff
```

### docker-dind by itself

```yaml
include:
- project: openraven/open/gitlab-ci
  file: docker-service-mixin.yml

a job that just needs dockerd:
  extends:
  - .docker-service-mixin
  script:
  - echo BUT THERE IS NO DOCKER CLI, YOU ARE ON YOUR OWN
```
